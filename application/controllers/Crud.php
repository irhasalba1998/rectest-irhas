<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Orm\User;
use Illuminate\Database\Capsule\Manager as DB;
use Illuminate\Database\Eloquent\Builder;

class Crud extends CI_Controller
{

    public function index()
    {
        $data['user'] = $this->db->get('user')->result();
        $this->load->view('user_page', $data);
    }

    public function create()
    {
        $data = $this->input->post("data");
        $data_insert = [
            'nama   ' => $data['nama'],
            'username' => $data['user'],
            'password' => sha1($data['pass']),

        ];
        $insert = $this->db->insert('user', $data_insert);
        // return redirect('user_page');
        return var_dump($data);
    }

    public function delete($user_id)
    {
        $delete = $this->db->where('id_user', $user_id)->delete('user');
        return redirect('crud/index');
    }
}
