<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Orm\User;
use Illuminate\Database\Capsule\Manager as DB;
use Illuminate\Database\Eloquent\Builder;

class Welcome extends CI_Controller
{

	public function index()
	{
		$data['user']    = User::all();
		$data['user_ci'] = $this->db->get('user')
			->result();
		$data['role'] = $this->db->select('nama_role,created_at')->get('role')->result();
		$data['user_role'] = DB::table('user_role')->join('role', 'user_role.id_role', '=', 'role.id_role')->join('user', 'user_role.id_user', '=', 'user.id_user')->select('user.nama', 'role.nama_role', 'user.created_at')->get();

		$this->load->view('rectest', $data);
	}
}
