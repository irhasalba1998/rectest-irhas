# Rekrutmen Test


1. Clone repository ini
2. Import database [database.sql](database.sql) pada mysql local anda
3. Configurasi koneksi project dengan database
4. Install vendor dengan composer
5. Jalankan project pada repository ini pada localhost anda, dapat menggunakan Xampp, php -S, dll
6. Setelah project running pada localhost anda, anda dapat mengakses aplikasi (contoh: http://localhost:8001/rectest/) pada browser anda
7. Silahkan kerjakan tugas sesuai yang tertulis pada soal di aplikasi. Contoh tampilan:

![alt text](assets/img/soal.png "Contoh interface aplikasi berisi soal")

8. Kumpulkan tugas sesuai mekanisme pengumpulan tugas (Tugas 3)
9. Waktu anda mengerjakan adalah 2 jam.
10. Selamat mengerjakan !
